/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.net.*;
import java.io.*;
import java.util.Scanner;
/**
 *
 * @author luisandrade
 */
public class Client {
    public static void main(String args[]){
        try {
            Socket cliente = new Socket ("", 12349);
            System.out.println("Conexao com servidor de verificao feita(porta 12345)...");	
           
            while(true){
                Scanner teclado = new Scanner(System.in);
                String mensagem = teclado.nextLine();
                DataOutputStream goToServer = new DataOutputStream(cliente.getOutputStream());
                goToServer.writeBytes(mensagem);
                goToServer.close();
            }
        }
    	catch(Exception e){
                System.out.println("ERRO: " + e.getMessage());
    	}
    }
}
