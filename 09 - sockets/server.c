#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
 
void error(const char *msg)
{
    perror(msg);
    exit(1);
}
 
int main(int argc, char *argv[])
{
    int sockfd, newsockfd, portno;
    socklen_t clilen;
    char buffer[256];
    struct sockaddr_in serv_addr, cli_addr;
    int n;
    if (argc < 2) {
        fprintf(stderr,"ERROR, no port provided\n");
        exit(1);
    }
    /*cria um socket de internet do tipo stream (tcp)*/
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0)
        error("ERROR opening socket");
    bzero((char *) &serv_addr, sizeof(serv_addr));
    portno = atoi(argv[1]);
    /**
     * inicializa a estrutura serv_addr no servidor:
     * sin_family = Internet
     * s_addr = INADDR_ANY, qualquer ip pode se conectar
     * sin_port = porta
     **/
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    serv_addr.sin_port = htons(portno);
    /*bind cria um servico na máquina corrente na porta especificada*/
    if (bind(sockfd, (struct sockaddr *) &serv_addr,
                sizeof(serv_addr)) < 0)
        error("ERROR on binding");
   
    /*enumera o número máximo de conexoes que esse servidor poderá receber
     * ao mesmo tempo*/
    listen(sockfd,5);
    clilen = sizeof(cli_addr);
 
    while(1){
        /*bloqueia até que uma nova conexao seja recebida*/
        newsockfd = accept(sockfd,
                (struct sockaddr *) &cli_addr,
                &clilen);
        if (newsockfd < 0)
            error("ERROR on accept");
        bzero(buffer,256);
       
        /*lê mensagens desse socket*/
        n = read(newsockfd,buffer,255);
        if (n < 0) error("ERROR reading from socket");
 
        struct sockaddr_in* pV4Addr = (struct sockaddr_in*)&cli_addr;
        struct in_addr ipAddr = pV4Addr->sin_addr;
        char ip_cliente[INET_ADDRSTRLEN];
        inet_ntop( AF_INET, &ipAddr, ip_cliente, INET_ADDRSTRLEN );
 
        printf("%s : %s\n",ip_cliente,buffer);
 
        /*escreve mensagens esse socket*/
        n = read(newsockfd,buffer,255);
        n = write(newsockfd,"I got your message",18);
        if (n < 0) error("ERROR writing to socket");
        close(newsockfd);
    }
 
    close(sockfd);
    return 0;
}