

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author luisandrade
 */
import java.net.*;
import java.io.*;
import java.util.Scanner;
public class Server {
    public static void main(String args[])throws IOException{
        
        //instaciando servidor, ouvindo a porta 12345
        ServerSocket servidor = new ServerSocket(12349);				
        System.out.println("ouvindo a porta 12349...");

        while(true){
            Socket cliente = servidor.accept();
            Scanner s = new Scanner(cliente.getInputStream());
            String x = s.nextLine();

            // Identify the data type
            if(x.contains(".")) {
                // String or Float
                try{
                    float value = Float.parseFloat(x);
                    System.out.println("<" + cliente.getInetAddress().getHostAddress() + ">" + ": " + x + " [FLOAT]");
                }
                catch(NumberFormatException erro)
                {
                    System.out.println("<" + cliente.getInetAddress().getHostAddress() + ">" + ": " + x + " [STRING]");
                }
            }
            else {
                // String or Int
                try{
                    int value = Integer.parseInt(x);
                    System.out.println("<" + cliente.getInetAddress().getHostAddress() + ">" + ": " + x + " [INT]");
                }
                catch(NumberFormatException erro)
                {
                    System.out.println("<" + cliente.getInetAddress().getHostAddress() + ">" + ": " + x + " [STRING]");
                }
            }    

            s.close();
            cliente.close();
        }
		
          
        //servidor.close();
    }    
}
