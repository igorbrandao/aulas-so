/*
 * Crie e compile um programa simples que implemente um loop infinito
 *
 * Verifique o programa executando na tabela de processos
*/
#include <iostream>

int main(void) {

	// Infinite loop
	while (true) {
		// system("sleep 1");
	}

	return 0;
}