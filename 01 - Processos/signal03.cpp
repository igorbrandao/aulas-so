/*
 * Crie um programa que inicialize 4 processos e:
 *
 * Imprima os pids dos processos filhos no console
 * Permita a um usuário controlar matar qualquer um dos 4 processos filhos (use a função kill do C).
 * Modifique o programa para se suicidar, usando um sinal, mediante uma entrada do usuário (use a função raise).
*/
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>

int main(int argc, char **argv)
{
    // Number of child processes
    int nChild = 4;

    // Array to receive child´s PID
    int childs[nChild] = { }; 

    // Create the parent process
    pid_t pid = 0;

    for (int i = 0; i < nChild; i++)
    {
        pid = fork();

        if (pid < 0) {
            printf("Error");
            exit(1);
        } 
        else if (pid == 0) {
            printf("Child (%d): %d - parent: %d\n", i + 1, getpid(), getppid());
            childs[i] = getpid();
            exit(0); 
        }
    }

    printf("Child process [%d] was killed!", childs[0]);
    kill(childs[0], SIGTERM);

    return 0;
}