/*
 * Crie um programa simples que fique em loop infinito em um laço com um sleep.
 *
 * a) Utilize a função signal para registrar um callback que espera por um determinado sinal. 
 * (Utilize kill –l para uma lista de sinais disponíveis).
 *
 * b) Modifique o programa usado para ser finalizado mediante à recepção de um sinal específico 
 * (Que não seja o SIG-KILL).
*/
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>

volatile sig_atomic_t flag = 0;

// Can be called asynchronously
void my_function(int sig){ 
	// Set flag
  	flag = 1;
}

int main(){

	// Register signal (ctrl+c) and signal handler
	signal(SIGINT, my_function); 

	printf("\n Program processing stuff here.\n");
	system("sleep 1");

	// Infinite loop
	while (true) 
	{
		if (flag) { 
			// Action when signal set it 1
			printf("\n Signal caught!\n");
			printf("\n default action it not termination!\n");
			flag = 0;
		}
	}

	return 0;
}  