/*
 * Crie outro programa que, antes do loop, use a chamada system 
 * para executar o programa anterior, criado no passo 1.
 *
 * Verifique a tabela de processos novamente
*/
#include <iostream>

int main(void) {

	// Call the previous program
	system("./fork01");

	while (true) {
		// system("sleep 1");
	}

	return 0;
}