#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdio.h>

#define SHMSZ     27

main()
{
	int shmid;
	key_t key;
	char *shm, *s;

	key = 5678;

	/*nesse caso, a memoria nao é criada*/
	if ((shmid = shmget(key, SHMSZ, 0666)) < 0) {
		perror("shmget");
		return 1;
	}

	/*traz a memoria compartilhada para dentro do espaco de enderecamento do programa*/
	shm = shmat(shmid, NULL, 0);

	if (shm == (void*) -1) {
		perror("shmat");
		return 1;
	}
	
	for (s = shm; *s != NULL; s++)
		putchar(*s);
	
	putchar('\n');
	
	/*coloca um * para indicar o fim da leitura*/
	*shm = '*';

	/*pode falhar, porque?*/
	if(shmdt(shm) != 0){
		perror("shmdet");
		return 1;
	}

	return 0;
}
