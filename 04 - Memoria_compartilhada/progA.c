/*
 * Crie dois programas, progA e progB, que contam de 1 até 10 indefinidamente, 
 * armazenando o contador em uma variável shared_cnt.
 * 
 * a) Utilize as chamadas de sistema shmget e shmat para compartilhar a variável 
 * shared_cnt entre os dois programas.
 *
 * b) Modifique os programas para imprimir, o valor de seu próprio contador e, em seguida 
 * imprimir o valor da variável shared_cnt (use dois prints).
*/
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>

#define SHMSZ     4

int main(){

	int i;
	int shmid;
	key_t key;
	int *shm, *shared_cnt;

	key = 5678;

	/*cria uma posi¢ao de memoria compartilhada com tamanho 4 bytes*/
	if ((shmid = shmget(key, SHMSZ, IPC_CREAT | 0666)) < 0) {
		perror("shmget");
		return 1;
	}

	/*traz a memoria compartilhada para dentro do espaco de enderecamento do programa*/
	shm = shmat(shmid, NULL, 0);

	if (shm == (void*) -1) {
		perror("shmat");
		return 1;
	}
	
	/*foi alocada uma regiao com 4 bytes*/
	shared_cnt = shm;

	for (i = 1; i <= 10; i++) {
		printf("progA -> i = %d, shared_cnt = %d\n", i, *shared_cnt);
		sleep(2);

		shared_cnt = &i;

		if (i == 10) {
			i = 0;
		}
  	}

	/*desanexa a posicao de memoria*/
	if(shmdt(shm) != 0){
		perror("shmdet");
		return 1;
	}

	shmctl(key, IPC_RMID, NULL);

	printf("%s terminado\n",__FILE__);

	return 0;
}