/*
 * Crie dois programas, progA e progB, que contam de 1 até 10 indefinidamente, 
 * armazenando o contador em uma variável shared_cnt.
 * 
 * a) Utilize as chamadas de sistema shmget e shmat para compartilhar a variável 
 * shared_cnt entre os dois programas.
 *
 * b) Modifique os programas para imprimir, o valor de seu próprio contador e, em seguida 
 * imprimir o valor da variável shared_cnt (use dois prints).
*/
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdio.h>

#define SHMSZ     4

main()
{
	int i;
	int shmid;
	key_t key;
	int *shm, *shared_cnt;

	key = 5678;

	/*nesse caso, a memoria nao é criada*/
	if ((shmid = shmget(key, SHMSZ, 0666)) < 0) {
		perror("shmget");
		return 1;
	}

	/*traz a memoria compartilhada para dentro do espaco de enderecamento do programa*/
	shm = shmat(shmid, NULL, 0);

	if (shm == (void*) -1) {
		perror("shmat");
		return 1;
	}

	shared_cnt = shm;

	for (i = 1; i <= 10; i++) {
		printf("progB -> i = %d, shared_cnt = %d\n", i, *shared_cnt);
		sleep(1);

		*shared_cnt = i;

		if (i == 10) {
			i = 0;
		}
  	}

	/*pode falhar, porque?*/
	if(shmdt(shm) != 0){
		perror("shmdet");
		return 1;
	}

	return 0;
}