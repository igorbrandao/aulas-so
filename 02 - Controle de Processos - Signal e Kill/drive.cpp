#include <iostream>
// #include <unistd>
#include <stdlib.h>
#include "signalHandler.hpp"
using namespace std;

int main()
{
	int iret;

	try
	{
		SignalHandler signalHandler;

		// Register signal handler to handle kill signal
		signalHandler.setupSignalHandlers();

		// Infinite loop until signal ctrl-c (KILL) received
		while(!signalHandler.gotExitSignal())
		{
		    system("sleep 1");
		}

		iret = EXIT_SUCCESS;
	}
	catch (SignalException& e)
	{
		std::cerr << "SignalException: " << e.what() << std::endl;
		iret = EXIT_FAILURE;
	}
	return(iret);
}