# README #
** Visão Geral**
O objetivo deste projeto é implementar uma multiplicação de matrizes com multi-processos.

### MEMBBROS DA EQUIPE ###
* Igor A. Brandão
* Luis Andrade

### COMO COMPILAR ###
* Use o makefile digitando o comando **'make'** pelo terminal, após ter navegado para a pasta do projeto.

### COMO EXECUTAR O PROGRAMA ###
Para executar o projeto é necessário chamar o arquivo executável após compilar com o comando **'make'** pelo terminal,assim:\n
* ./bin/matrix

Após a execução, um menu com as seguintes opções aparecerá:

1. **Multiply matrix single process** => Realiza uma operação simples de multiplicação de matrizes com 1 processo
2. **Multiply matrix multiple processes**	=> Realiza uma operação simples de multiplicação de matrizes com N processos
3. **Benchmark between single process and multiple processes** => Realiza um teste comparativo entre multiplicação de matrizes com 1 e N processos. Gera como resultado 3 arquivos.
4. **Quit** => Encerra a aplicação

Os arquivos de saída do benchmark encontram-se na pasta */assets/output*:

1. **<nome_do_arquivo>** => log de todas as operações (print das matrizes);\n
2. **<nome_do_arquivo>SINGLE** => tempos de execução das multiplicações com 1 processo;\n
3. **<nome_do_arquivo>MULTIPLE** => tempos de execução das multiplicações com N processos;\n

### LISTA DE CLASSES ###
As classes utilizadas pelo programa são as seguintes:

**Matrix.h** 		=> Provê a definição geral da estrutura de dados da matrix.\n
**Matrix.inl** 		=> Implementa as funções definidas na classe Matrix.h.\n
**drive.cpp** 		=> Realiza as chamadas aos métodos da classe Matrix.h (ignição do sistema).\n
