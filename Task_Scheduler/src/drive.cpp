/*! \file */ 
/*! \brief main.cpp.
 *
 *  Starts the program.
*/
#include <iostream>
#include "Matrix.h"
#include <ctime>
#include <chrono>
#include <fstream>
#include <string.h>

using namespace std;
using namespace std::chrono;

//! Functions prototypes
void return_to_menu();
void displayMainMenu();

//! File handlers
fstream file;
fstream file2;
fstream file3;
string fname;

/**
 * Get the user choice
*/
int getInput()
{
    int choice;
    cin >> choice;
    return choice;
}

/**
 * Perform the matrix multiplication with a single process and multiple processes
 * 
 * This function do not save any log in file, just display the result
 *
 * @return void
*/
void singleTest()
{
    /*! Variables to receive the commands parameter */
    int itemValue;

    do
    {
        /*! Check if all arguments was passed */
        cout << endl << "<<< Inform the matrix order ('q' to return): ";

        if ( scanf("%i", &itemValue) == 1 )
        {
            float durationSingle, durationMultiple = 0;

            int rows = itemValue;
            int columns = itemValue;

            //! Matrix instances
            Matrix<int> matrixA(rows,columns);
            Matrix<int> matrixB(rows,columns);
            Matrix<int> matrixC(rows,columns);

            /*! Matrix to store the shared matrix */
            Matrix<int> sharedMatrix(rows,columns);

            //! Fill the matrix
            matrixA.fillMatrix();
            matrixB.fillMatrix();

            //! =============================================
            //! SINGLE PROCESS MULTIPLICATION
            //! =============================================

            //! Initial time
            high_resolution_clock::time_point st1 = high_resolution_clock::now();

            // Perform multiply operation (single process)
            matrixC = matrixC.multiplyMatrix(matrixA, matrixB);

            //! Finishing time
            high_resolution_clock::time_point st2 = high_resolution_clock::now();

            //! Calculates the processing duration
            durationSingle = duration_cast<microseconds>( st2 - st1 ).count();

            //! =============================================
            //! MULTIPLE PROCESSES MULTIPLICATION
            //! =============================================

            //! Initial time
            high_resolution_clock::time_point mt1 = high_resolution_clock::now();

            // Perform multiply operation
            sharedMatrix = matrixC.multiplyMatrixMultipleProcesses(matrixA, matrixB);

            //! Finishing time
            high_resolution_clock::time_point mt2 = high_resolution_clock::now();

            //! Calculates the processing duration
            durationMultiple = duration_cast<microseconds>( mt2 - mt1 ).count();

            // Print matrix A
            cout << "\n==============================================\n";
            cout << "*** MATRIX A ***\n";
            cout << "==============================================\n";
            matrixA.print();
            cout << "==============================================\n";

            // Print matrix B
            cout << "\n==============================================\n";
            cout << "*** MATRIX B ***\n";
            cout << "==============================================\n";
            matrixB.print();
            cout << "==============================================\n";

            // Print matrix C
            cout << "\n==============================================\n";
            cout << "*** MATRIX C ***\n";
            cout << "==============================================\n";
            sharedMatrix.print();
            cout << "==============================================\n";

            // Print the time to perform the operation
            cout << endl << "Single operation executed in: " << durationSingle << " Microseconds " << endl;
            cout << endl << "Multiple operation executed in: " << durationMultiple << " Microseconds " << endl;
            }
    }
    while ( getchar() != 'q' );

    displayMainMenu();
}

/**
 * Perform the matrix multiplication with a single process
 *
 * @param isMultipleProcesses => flag to define if the multiplication
 * will be handled with a single process (false) or multiple (true)
 *
 * @return void
*/
void matrixMultiplication(bool isMultipleProcesses)
{
    /*! Variables to receive the commands parameter */
    int itemValue;

    do
    {
        /*! Check if all arguments was passed */
        cout << endl << "<<< Inform the matrix order ('q' to return): ";

        if ( scanf("%i", &itemValue) == 1 )
        {
            float duration = 0;

            //! Matrix instances
            Matrix<int> matrixA(itemValue,itemValue);
            Matrix<int> matrixB(itemValue,itemValue);
            Matrix<int> matrixC(itemValue,itemValue);

            /*! Matrix to store the shared matrix */
            Matrix<int> sharedMatrix(itemValue,itemValue);

            //! Fill the matrix
            matrixA.fillMatrix();
            matrixB.fillMatrix();

            //! Single process
            if (isMultipleProcesses == false)
            {
                //! Initial time
                high_resolution_clock::time_point t1 = high_resolution_clock::now();

                // Perform multiply operation
                matrixC = matrixC.multiplyMatrix(matrixA, matrixB);

                //! Finishing time
                high_resolution_clock::time_point t2 = high_resolution_clock::now();

                //! Calculates the processing duration
                duration = duration_cast<microseconds>( t2 - t1 ).count();
            }
            //! Multiple processes
            else
            {
                //! Initial time
                high_resolution_clock::time_point t1 = high_resolution_clock::now();

                // Perform multiply operation
                sharedMatrix = matrixC.multiplyMatrixMultipleProcesses(matrixA, matrixB);

                //! Finishing time
                high_resolution_clock::time_point t2 = high_resolution_clock::now();

                //! Calculates the processing duration
                duration = duration_cast<microseconds>( t2 - t1 ).count();
            }

            
            // Print matrix A
            cout << "\n==============================================\n";
            cout << "*** MATRIX A ***\n";
            cout << "==============================================\n";
            matrixA.print();
            cout << "==============================================\n";

            // Print matrix B
            cout << "\n==============================================\n";
            cout << "*** MATRIX B ***\n";
            cout << "==============================================\n";
            matrixB.print();
            cout << "==============================================\n";

            // Print matrix C
            cout << "\n==============================================\n";
            cout << "*** MATRIX C ***\n";
            cout << "==============================================\n";
            matrixC.print();
            cout << "==============================================\n";

            // Print the time to perform the operation
            cout << endl << "Operation executed in: " << duration << " Microseconds " << endl;

        }
    }
    while ( getchar() != 'q' );

    displayMainMenu();
}

/**
 * Perform a benchmark between the multiplication with single and multiple processes
*/
void benchmark()
{
    /*! Variables to receive the commands parameter */
    int itemValue;

    do
    {
        /*! Check if all arguments was passed */
        cout << endl << "<<< Inform the matrix order limit [2 - limit] ('q' to return): ";

        if ( scanf("%i", &itemValue) == 1 )
        {
            float durationSingle, durationMultiple = 0;

            cout << endl << "<<< Inform the automatic benchmark filename: ";
            cin >> fname;
            cout << endl;

            /*! Try to create a benchmark file */
            file.open("assets/output/" + fname,fstream::out);
            file2.open("assets/output/" + fname + "SINGLE",fstream::out);
            file3.open("assets/output/" + fname + "MULTIPLE",fstream::out);
            cout << endl << "<<< Generating automatic benchmark file... >>>" << endl << endl;

            for (int index = 2; index <= itemValue; index++)
            {
                //! Matrix instances
                Matrix<int> matrixA(index,index);
                Matrix<int> matrixB(index,index);
                Matrix<int> matrixC(index,index);

                /*! Matrix to store the shared matrix */
                Matrix<int> sharedMatrix(index,index);

                //! Fill the matrix
                matrixA.fillMatrix();
                matrixB.fillMatrix();

                //! =============================================
                //! SINGLE PROCESS MULTIPLICATION
                //! =============================================

                //! Initial time
                high_resolution_clock::time_point st1 = high_resolution_clock::now();

                // Perform multiply operation (single process)
                matrixC = matrixC.multiplyMatrix(matrixA, matrixB);

                //! Finishing time
                high_resolution_clock::time_point st2 = high_resolution_clock::now();

                //! Calculates the processing duration
                durationSingle = duration_cast<microseconds>( st2 - st1 ).count();

                //! Write into the log the processing time
                file2 << "Order " << index << ": " << durationSingle << " ms;\n";

                //! =============================================
                //! MULTIPLE PROCESSES MULTIPLICATION
                //! =============================================

                //! Initial time
                high_resolution_clock::time_point mt1 = high_resolution_clock::now();

                // Perform multiply operation (single process)
                sharedMatrix = matrixC.multiplyMatrixMultipleProcesses(matrixA, matrixB);

                //! Finishing time
                high_resolution_clock::time_point mt2 = high_resolution_clock::now();

                //! Calculates the processing duration
                durationMultiple = duration_cast<microseconds>( mt2 - mt1 ).count();

                //! Write into the log the processing time
                file3 << "Order " << index << ": " << durationMultiple << " ms;\n";

                //! =============================================
                //! WRITE THE LOG FILE
                //! =============================================

                matrixC.printFile(file, matrixA, matrixB, matrixC);
            }

            cout << endl << "<<< Benchmark file generated with success... >>>" << endl << endl;
            file.close();
            file2.close();
            file3.close();
        }
    }
    while ( getchar() != 'q' );

    displayMainMenu();
}

/**
 * Handle the user input choice
*/
void inputHandler( int choice_ )
{
    switch( choice_ )
    {
        /*! Multiply matrices with a single process */
        case 1:
            singleTest();
            break;

        /*! Multiply matrices with a single process */
        case 2:
            matrixMultiplication(false);
            break;

        /*! Multiply matrices with multiple processes */
        case 3:
            matrixMultiplication(true);
            break;

        /*! Perform a benchmark test */
        case 4:
            benchmark();
            break;

        /*! Exit */
        case 5:
            exit(0);
            break;

        default:
            displayMainMenu();
            break;
    }
}

/**
 * Display the menu
*/
void displayMainMenu()
{
    system("clear");

    cout << endl << "==============================================================================" << endl;
    cout << "*** Matrix multiply v0.1 ***" << endl;
    cout << "==============================================================================" << endl << endl;
    cout << "Please make your selection:\n\n";
    cout << "1 - Single matrix multiplication comparation test\n";
    cout << "2 - Multiply matrix single process\n";
    cout << "3 - Multiply matrix multiple processes\n";
    cout << "4 - Benchmark between single process and multiple processes\n";
    cout << "5 - Quit\n";
    cout << endl << "==============================================================================" << endl << endl;
    cout << "Choose an option: ";

    inputHandler(getInput());
}

/**
 * Function to return to menu
*/
void return_to_menu()
{
    char opt;
    cout << endl << "Would you like to return to main menu? (y or n): ";
    cin >> opt;

    if ( opt == 'y' || opt == 'Y' )
        displayMainMenu();
    else
        exit(0);
}

/********************************************//**
* Main
***********************************************/
int main( void )
{
    //! Display the application menu
    displayMainMenu();

    //! Status message
    cout << "<<< Finish with success! >>>" << endl << endl;

    /*! Main return */
    return 0;
}