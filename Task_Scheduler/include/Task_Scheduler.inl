/*! \file */ 
/*! \brief Task_Scheduler.cpp.
 *
 *  Implements the functions from Tree class.
*/

#include "Task_Scheduler.h"
#include <cstdlib>
#include <ctime>
#include <stdlib.h>
#include <iostream>

using namespace std;

/*!
 * Class constructor
 * Initialize the Task_Scheduler
 *
 * @return => void
*/
template< typename T >
Task_Scheduler<T>::Task_Scheduler( int rows, int cols )
{
	// Set the rows and columns
	setRow(rows);
	setCol(cols);

	// Allocates the new template Task_Scheduler rows
	data = new T*[rows];

	// Allocate each column
	for (int i = 0; i < row; i++) {
		data[i] = new T [cols];
	}
}

/*!
 * Class destructor
 * realeases the allocated Task_Scheduler block
 *
 * @return => void
*/
template<class T>
Task_Scheduler<T>::~Task_Scheduler()
{
	// TODO
}

/*!
 * Fill Task_Scheduler function
 * inserts random numbers into Task_Scheduler positions 
 *
 * @param Task_Scheduler_ => the Task_Scheduler instance
 *
 * @return => void
*/
template<class T>
void Task_Scheduler<T>::fillTask_Scheduler()
{
	// Limiters
	T min = 1;
	T max = 100;

	// Randomic variable
	T randNum;

	// Fill the Task_Scheduler with random numbers
	for(int i = 0; i < row; i++) {
        for(int j = 0; j < col; j++) {
        	// Genrate a random value
        	randNum = rand()%(max-min + 1) + min;

        	// Attribute it to the Task_Scheduler position
           	data[i][j] = randNum;
        }
    }
}

/*!
 * Fill Task_Scheduler function
 * inserts random numbers into Task_Scheduler positions 
 *
 * @param Task_Scheduler1_ => first Task_Scheduler instance
 * @param Task_Scheduler2_ => second Task_Scheduler instance
 *
 * @return => Task_Scheduler<T>
 *
 * reference: https://www.programiz.com/cpp-programming/examples/Task_Scheduler-multiplication
*/
template <class T>
Task_Scheduler<T> Task_Scheduler<T>::multiplyTask_Scheduler( Task_Scheduler Task_Scheduler1_, Task_Scheduler Task_Scheduler2_ )
{
	// Creat a new Task_Scheduler resultant
	Task_Scheduler<T> result(Task_Scheduler1_.getRow(),Task_Scheduler2_.getCol());
	
	// Proceed the matrices multiplication operation
	for (int i = 0; i < Task_Scheduler1_.getRow(); ++i)
	{
		for (int j = 0; j < Task_Scheduler2_.getCol(); ++j)
		{
			for (int k = 0; k < Task_Scheduler1_.getCol(); ++k)
            {
                result.data[i][j] += Task_Scheduler1_.data[i][k] * Task_Scheduler2_.data[k][j];
            }
		}
	}
	
	return result;
}

/*!
 * Fill Task_Scheduler function
 * inserts random numbers into Task_Scheduler positions
 * 
 * In order to be more efficient, this method should split
 * each column x row multiplication with one unique process
 * and the main process needs to print the final result
 *
 * @param Task_Scheduler1_ => first Task_Scheduler instance
 * @param Task_Scheduler2_ => second Task_Scheduler instance
 *
 * @return => Task_Scheduler<T>
*/
template <class T>
Task_Scheduler<T> Task_Scheduler<T>::multiplyTask_SchedulerMultipleProcesses( Task_Scheduler Task_Scheduler1_, Task_Scheduler Task_Scheduler2_ )
{
	// Create a shared Task_Scheduler instance
	Task_Scheduler mem;

	int n = Task_Scheduler1_.getCol();

    // Create the shared Task_Scheduler itself
    T *Task_Scheduler3 = mem.attachSharedTask_Scheduler(n);

    // Creat a new Task_Scheduler resultant
    Task_Scheduler<T> result(Task_Scheduler1_.getRow(),Task_Scheduler2_.getCol());

    // fork here
    pid_t pid;
    pid = fork();

    if (pid < 0)
    {
        // if error, fork failed
        fprintf(stderr, "Fork Failed");
        exit(-1);
    }
    else if (pid == 0)
    {
        // fork here
        pid_t pid2;
        pid2 = fork();

        if (pid2 < 0)
        {
            // if error, fork failed
            fprintf(stderr, "Fork Failed");
            exit(-1);
        }
        else if (pid2 == 0)
        {
            // child, and calc the first part, i=0 ~ n/2
            for (int i = 0; i < Task_Scheduler1_.getRow()/2; i++)
            {
                for (int j = 0; j < Task_Scheduler1_.getCol()/2 ; j++)
                {
                    Task_Scheduler3[i*n + j] = 0;
                    for (int k = 0; k < Task_Scheduler1_.getCol(); k++)
                    {
						Task_Scheduler3[i*n + j] += (Task_Scheduler1_.data[i][k] * Task_Scheduler2_.data[k][j]);
                    }
                }
            }

            mem.detachSharedTask_Scheduler();
            exit(0);
        }
        else
        {
            // parent, i=n/2 ~ n
            for (int i = n/2; i < Task_Scheduler1_.getRow(); i++)
            {
            	for (int j = 0; j < Task_Scheduler1_.getCol()/2 ; j++)
                {
                	Task_Scheduler3[i*n + j] = 0;
                    for(int k = 0; k < Task_Scheduler1_.getCol(); k++)
                    {
                        Task_Scheduler3[i*n + j] += (Task_Scheduler1_.data[i][k] * Task_Scheduler2_.data[k][j]);
                    }
                }
            }

            wait(NULL);
        }

        mem.detachSharedTask_Scheduler();
        exit(0);
    }
    else
    {
        // fork here
        pid_t pid3;
        pid3 = fork();

        if (pid3 < 0)
        {
            // if error, fork failed
            fprintf(stderr, "Fork Failed");
            exit(-1);
        }
        else if (pid3 == 0)
        {
            // child, and calc the first part, i=0 ~ n/2
            for(int i = 0; i < Task_Scheduler1_.getRow()/2; i++)
            {
                for (int j = Task_Scheduler1_.getCol()/2 ; j < Task_Scheduler1_.getCol() ; j++ ){
                    Task_Scheduler3[i*n + j] = 0;
                    for(int k = 0; k < Task_Scheduler1_.getCol(); k++)
                    {
                        Task_Scheduler3[i*n + j] += (Task_Scheduler1_.data[i][k] * Task_Scheduler2_.data[k][j]);
                    }
                }
            }

            mem.detachSharedTask_Scheduler();
            exit(0);
        }
        else
        {
            // parent, i=n/2 ~ n
            for(int i = Task_Scheduler1_.getRow()/2; i < Task_Scheduler1_.getRow(); i++)
            {
                for(int j = Task_Scheduler1_.getCol()/2; j < Task_Scheduler1_.getCol(); j++)
                {
                    Task_Scheduler3[i*n + j] = 0;
                    for(int k = 0; k < Task_Scheduler1_.getCol(); k++)
                    {
                        Task_Scheduler3[i*n + j] += (Task_Scheduler1_.data[i][k] * Task_Scheduler2_.data[k][j]);
                    }
                }
            }

            // if parent
            wait(NULL);
            wait(NULL);

            // result up here
            for(int i = 0; i < Task_Scheduler1_.getRow(); i++)
            {
                for(int j = 0; j < Task_Scheduler1_.getCol(); j++)
                {
                    result.data[i][j] = Task_Scheduler3[Task_Scheduler1_.getRow()*i + j];
                }
            }

            mem.detachAndReleaseSharedTask_Scheduler();
        }
    }

    return result;
}

/*!
 * Print function
 * prints the entire Task_Scheduler
 *
 * @return => void
*/
template <class T>
void Task_Scheduler<T>::print() const
{
    // Run through Task_Scheduler rows
    for (int i = 0; i < row; i++)
    {
    	// Run through Task_Scheduler columns
        for(int j = 0;j < col; j++)
        {
            cout << data[i][j] << "\t";
        }

        printf("\n");
    }
}

/*!
 * Print file function
 * prints the entire Task_Scheduler
 *
 * @return => void
*/
template <class T>
void Task_Scheduler<T>::printFile( fstream& file, Task_Scheduler Task_Scheduler1_, Task_Scheduler Task_Scheduler2_, Task_Scheduler Task_Scheduler3_ ) const
{
    file << "================================================================\n";
    file << "Task_Scheduler order: " << Task_Scheduler1_.getRow() << "\n";
    file << "================================================================\n";

    //! Write in log file
    file << "\n----------------------------------------------------\n";
    file << "Task_Scheduler A";
    file << "\n----------------------------------------------------\n";
    for(int r = 0; r < Task_Scheduler1_.getRow(); ++r)
    {
        for(int c = 0; c < Task_Scheduler1_.getCol(); ++c)
        {
            file << Task_Scheduler1_.data[r][c] << "\t";
        }

        file << "\n";
    }
    file << "----------------------------------------------------\n";

    file << "\n" << "\t X \n";

    //! Write in log file
    file << "\n----------------------------------------------------\n";
    file << "Task_Scheduler B";
    file << "\n----------------------------------------------------\n";
    for(int r = 0; r < Task_Scheduler2_.getRow(); ++r)
    {
        for(int c = 0; c < Task_Scheduler2_.getCol(); ++c)
        {
            file << Task_Scheduler2_.data[r][c] << "\t";
        }

        file << "\n";
    }
    file << "----------------------------------------------------\n";

    file << "\n" << "\t = \n";

    //! Write in log file
    file << "\n----------------------------------------------------\n";
    file << "Task_Scheduler C";
    file << "\n----------------------------------------------------\n";
    for(int r = 0; r < Task_Scheduler3_.getRow(); ++r)
    {
        for(int c = 0; c < Task_Scheduler3_.getCol(); ++c)
        {
            file << Task_Scheduler3_.data[r][c] << "\t";
        }

        file << "\n";
    }
    file << "----------------------------------------------------\n\n";
}

/*!
 * Returns a Task_Scheduler copy
 *
 * @return => <T>
*/
template<class T> 
T& Task_Scheduler<T>::operator()(int row, int col)
{
    return data[row][col];
}