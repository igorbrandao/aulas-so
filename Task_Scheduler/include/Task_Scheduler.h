/*!
    <PRE>
        SOURCE FILE : Task_Scheduler.h
        DESCRIPTION.: Task_Scheduler class - with implementation (Task_Scheduler.inl).
        AUTHORS.....: Igor A. Brandão and Luis Andrade
        CONTRIBUTORS: Igor A. Brandão and Luis Andrade
        LOCATION....: IMD/UFRN.
        SATARTED ON.: NOV/2017
        CHANGES.....: Structure and functions implemented.

        TO COMPILE..: Use makefile.
        OBS.........: Part of the SO Project.

        LAST UPDATE.: Nov 04th, 2017.
    </PRE>
*/

#ifndef Task_Scheduler_H_
#define Task_Scheduler_H_

#include <iostream>
#include <fstream>
#include <string.h>

using namespace std;

/*! Task_Scheduler class */
template < typename T >
class Task_Scheduler
{
    /*!
     * Protected section
    */
    protected:

        // Task structure
        typedef struct task_t {
            
            // Function to be executed like a process
            void* (*func)(void*);

            // PID (process ID)
            int id;

            // Process priority
            int p;

            // Calculated quantum throughout the active scheduler politics
            int quantum;
        } task;

        // Scheduler type enum
        typedef enum {
            NORMAL, 
            FIFO,
            RR
        } scheduler_type;

    /*!
     * Public section
    */
    public:

        /*! Class constructor to create an empty Task_Scheduler */
        Task_Scheduler();

        /*! Class destructor to release memory */
        ~Task_Scheduler();

        /*
         * @brief Fuçao que inicializa uma tarefa (processo) específica.
         * @param task_ a tarefa a ser executada.
         * @return 0 em caso de sucesso, -1 em caso de falha.
         */
        int start_task (task *task_)

        /*
         * @brief Fuçao que recupera a estrutura dentro do escalonador.
         * @param id identificador da task desejada
         * @return uma referencia para a estrutura task em caso de sucesso, NULL em caso de falha.
         */
        task* get_task (int id)


        /*
         * @brief Muda o tipo de escalonador usado para um dos tipos específicos (NORMAL, FIFO e RR).
         * @param sheduler tipo do escalonador desejado.
         * @return 0 em caso de sucesso, -1 em caso de falha.
         */
        int set_scheduler_type(scheduler_type scheduler)
};

#include "Task_Scheduler.inl"
#endif // Task_Scheduler_H

/* --------------------- [ End of the Task_Scheduler.h header ] ------------------- */
/* ============================================================================== */