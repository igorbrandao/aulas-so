/*!
    <PRE>
        SOURCE FILE : Memory.h
        DESCRIPTION.: Memory class - with implementation.
        AUTHORS.....: Igor A. Brandão and Luis Andrade
        CONTRIBUTORS: Igor A. Brandão and Luis Andrade
        LOCATION....: IMD/UFRN.
        SATARTED ON.: SET/2017
        CHANGES.....: Structure and functions implemented.

        TO COMPILE..: Use makefile.
        OBS.........: Part of the SO Project.

        LAST UPDATE.: Set 08th, 2017.
    </PRE>
*/

#ifndef Memory_H_
#define Memory_H_

#include <stdio.h>
#include <unistd.h>
#include <iostream>
#include <sys/types.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <sys/ipc.h>
#include <sys/wait.h>

using namespace std;

/*! Memory class */
class Memory 
{
    /*!
     * Private section
    */
    private:
        int shmid;
        int *sharedMemory;

    /*!
     * Public section
    */
    public:

        void detachAndReleaseSharedMemory()
        {
            detachSharedMemory();

            if (shmctl(shmid, IPC_RMID, 0) == -1) {
                fprintf(stderr, "shmctl(IPC_RMID) failed\n");
                exit(1);
            }
        }

        void detachSharedMemory()
        {
            if (shmdt(sharedMemory) == -1) {
                fprintf(stderr, "shmdt failed\n");
                exit(EXIT_FAILURE);
            }
        }

        int *attachSharedMemory(int n)
        {
            // setup shared memory
            key_t key = 0x6060;

            // Create a memory posix with rows * cols bytes
            if ((shmid = shmget(key, (sizeof(int) * n * n), IPC_CREAT | 0666)) < 0) {
                perror("shmget: shmget failed");
                exit(1);
            }

            // Bring the shared memory to the program space address
            sharedMemory = (int*)shmat(shmid, NULL, 0);

            // Check if the shared memory was created
            if (sharedMemory == (void*) -1) {
                perror("shmat: attach error");
                exit(1);
            }

            return sharedMemory;
        }
};

#endif // Memory_H

/* --------------------- [ End of the Memory.h header ] ------------------- */
/* ============================================================================== */