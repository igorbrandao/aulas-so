/*
 * Aula 30/08/2017
 * Desafio 02
 * 
 * Crie dois programas, progA e progB, que contam de 1 até 10 indefinidamente, 
 * armazenando o contador em uma variável shared_cnt.
 * 
 * a) Utilize as chamadas de sistema shmget e shmat para compartilhar a variável 
 * shared_cnt entre os dois programas.
 *
 * b) Modifique os programas para imprimir, o valor de seu próprio contador e, em seguida 
 * imprimir o valor da variável shared_cnt (use dois prints).
 *
 * c) Implemente um vetor de char com 10 posicoes, onde o progA escreve o caractere ´a´ e
 * e o progB escreve ´b´ por meio de um buffer compartilhado.
 *
 * d) Imprima o vetor de chars e demonstre a sobrescrita de cada programa sobre o vetor.
*/
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>

#define SHMSZ     	(int)(10 * sizeof(int))
#define SHMCHAR     10

int main(){

	int i, j;
	int shmid, shmcharid;
	key_t key, key2;
	int *shm, *shared_cnt;
	char *shmchar, *shared_char;

	key = 0x6060;
	key2 = 0x6061;

	/*cria uma posi¢ao de memoria compartilhada com tamanho 40 bytes*/
	if ((shmid = shmget(key, SHMSZ, IPC_CREAT | 0666)) < 0) {
		perror("shmget");
		return 1;
	}

	/*cria uma posi¢ao de memoria compartilhada com tamanho 10 bytes*/
	if ((shmcharid = shmget(key2, SHMCHAR, IPC_CREAT | 0666)) < 0) {
		perror("shmget");
		return 1;
	}

	/*traz a memoria compartilhada para dentro do espaco de enderecamento do programa*/
	shm = shmat(shmid, NULL, 0);
	shmchar = shmat(shmcharid, NULL, 0);

	if (shm == (void*) -1) {
		perror("shmat");
		return 1;
	}

	if (shmchar == (void*) -1) {
		perror("shmat");
		return 1;
	}
	
	/*foi alocada uma regiao com 4 bytes*/
	shared_cnt = shm;

	/*foi alocada uma regiao com 10 bytes*/
	shared_char = shmchar;

	/* limpa o vetor */
	for (i = 1; i <= 10; i++) {
		shared_char[(i-1)] = 'x';
	}

	for (i = 1; i <= 10; i++) {
		shared_cnt[(i-1)] = i;
		shared_char[(i-1)] = 'a';

		printf("progA -> i = %d, shared_cnt = %d\n", i, shared_cnt[(i-1)]);

		printf("progA -> i = %d, shared_char = [", i);
		for (j = 1; j < 10; j++) {
			printf("%c,", shared_char[(j-1)]);
		}
		printf("]\n");
		sleep(2);

		if (i == 10) {
			i = 0;
		}
  	}

	/*desanexa a posicao de memoria*/
	if(shmdt(shm) != 0){
		perror("shmdet");
		return 1;
	}

	shmctl(key, IPC_RMID, NULL);

	printf("%s terminado\n",__FILE__);

	return 0;
}