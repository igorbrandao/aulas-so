/*	
	Name    : Divyen K Patel	Roll No:24		MCA-4
	Program : 2
	Header files: process creation and process joining
*/

int process_fork(int newproc)
{
	int i;
	for(i=1;i<newproc;i++)
	{
		if(fork()==0)
			return i;
	}
	return 0;
}

int process_join(int newproc,int id)
{
	int i;
	if(id==0)
	{
		for(i=1;i<newproc;i++)
			wait(0);
	}
	else
		exit(0);
}