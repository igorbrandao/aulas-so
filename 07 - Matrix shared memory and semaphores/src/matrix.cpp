#include "stdio.h"
#include <sys/types.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <sys/ipc.h>
#include "forkjoin.h"
#include "sharedlib.h"
#include "spinlock.h"

int main()
{

    int arr1[10][10];
    // Matrix Array

    int arr2[10];
    // Vector Array - Singular Column Matrix

    int* ansArr;
    // Multipication Answer Stored Here

    // It need to be shared

    int shmid;
    // For ansArr

    int r1, c1;
    // Number of Rows and Columns of First Matrix

    int r2, c2 = 1;
    // Number of Rows and Columns of Second Matrix

    int iCount, jCount;

    int id;
    // Stores Process ID

    int nproc;
    // Number of Processes

    int sum;

    printf("Enter Number of Rows of First Matrix :");

    scanf("%d", &r1);

    printf("Enter Number of Columns of First Matrix :");

    scanf("%d", &c1);

    printf("Enter Number of Rows of Second Matrix :");

    scanf("%d", &r2);

    if (c1 != r2)

    {

        printf("Matrix Multipication is not Possible ...");

        exit(1);
    }

    // Initialize an Array

    printf("\n\nEnter Values for Matrix ...\n");

    for (iCount = 0; iCount < r1; iCount++)

    {

        for (jCount = 0; jCount < c1; jCount++)

        {

            printf("Enter Value for Arr1[%d][%d] :", iCount, jCount);

            scanf("%d", &arr1[iCount][jCount]);
        }
    }

    printf("\n\nEnter Values for Vector Matrix ...\n");

    for (iCount = 0; iCount < r2; iCount++)

    {

        printf("Enter Value for Arr2[%d] :", iCount);

        scanf("%d", &arr2[iCount]);
    }

    ansArr = (int*)sshared(sizeof(int) * r1, &shmid);

    nproc = r1;
    // Processes for Each Row Multipication

    id = process_fork(nproc);
    sum = 0;

    for (iCount = 0; iCount < c1; iCount++)

    {

        sum += (arr1[id][iCount] * arr2[iCount]);
    }
    ansArr[id] = sum;

    process_join(nproc, id);

    printf("Array 1\n");

    for (iCount = 0; iCount < r1; iCount++)

    {
        for (jCount = 0; jCount < c1; jCount++)

        {

            printf("%d\t", arr1[iCount][jCount]);
        }

        printf("\n");
    }

    printf("Array 2 (Vector Matrix) \n");

    for (iCount = 0; iCount < r1; iCount++)

    {
        printf("%d\n", arr2[iCount]);
    }

    printf("Matrix-Vector Multipication \n");

    for (iCount = 0; iCount < r1; iCount++)

    {

        printf("%d\n", ansArr[iCount]);
    }

    cleanup_memory(&shmid);

    return 0;
}
---------------------------------------------------- -

    /*
create and remove shared memory block 
*/
    void* sshared(int size, int size, int* shmid)
{

    *shmid = shmget(IPC_PRIVATE, size, 0666 | IPC_CREAT);

    if (*shmid < 0)

    {

        printf("Error, cannot share memory");

        exit(0);
    }

    return shmat(*shmid, 0, 0);
}

void cleanup_memory(int* shmid)

{

    struct shmid_ds* buf;

    if (shmctl(*shmid, IPC_RMID, buf) != 0)

    {

        printf("Error, cannot free memory");

        exit(0);
    }
}
------------------------------------------------
/* 
 * for locking the critical region
*/

void spin_lock_init(int *lok)

{
    int init_sem_value = 1;
    *lok = semget(IPC_PRIVATE, 1, (0600 | IPC_CREAT));

    if (*lok == -1) {
        perror("Error in Semget ...");
        exit(1);
    }

    if (semctl(*lok, 0, SETVAL, init_sem_value) < 0) {
        perror("Error in Semctl ...");
        exit(1);
    }
}

void spin_lock(int* lok)
{
    struct sembuf sembuffer, *sops;
    sops = &sembuffer;
    sops->sem_num = 0;
    sops->sem_op = -1;
    sops->sem_flg = 0;

    if (semop(*lok, sops, 1) < 0) {
        perror("semop lock Error ...");
        exit(1);
    }
}
void spin_unlock(int* lok)
{
    struct sembuf sembuffer, *sops;
    sops = &sembuffer;
    sops->sem_num = 0;
    sops->sem_op = 1;
    sops->sem_flg = 0;

    if (semop(*lok, sops, 1) < 0) {
        perror("semop Unlock Error ...");
        exit(1);
    }
}

void cleanup_semaphore(int* lok)
{
    if (semctl(*lok, 1, IPC_RMID, 0) < 0) {
        perror("cleaning");
    }
}
---------------------------------------------------- -

    /* 
process creation and process joining
*/

    int process_fork(int newproc)
{

    int i;

    for (i = 1; i < newproc; i++)

    {

        if (fork() == 0)

            return i;
    }

    return 0;
}

int process_join(int newproc, int id)

{

    int i;

    if (id == 0)

    {

        for (i = 1; i < newproc; i++)

            wait(0);
    }

    else

        exit(0);
}