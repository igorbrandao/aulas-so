/*! \file */ 
/*! \brief main.cpp.
 *
 *  Starts the program.
*/
#include <iostream>
#include "Matrix.h"
#include <ctime>
#include <chrono>
#include <fstream>
#include <string.h>

//! Functions prototypes
void return_to_menu();
void displayMainMenu();

/**
 * Get the user choice
*/
int getInput()
{
    int choice;
    cin >> choice;
    return choice;
}


/**
 * Perform a benchmark between the multiplication with single and multiple processes
*/
void createProcess()
{
    /*! Variables to receive the commands parameter */
    int itemValue;

    do
    {
        /*! Check if all arguments was passed */
        cout << endl << "<<< Inform any number to create a process [1 - limit] ('q' to return): ";

        if ( scanf("%i", &itemValue) == 1 )
        {
            // fork here
            pid_t pid;
            pid = fork();

            if (pid < 0)
            {
                // if error, fork failed
                fprintf(stderr, "Fork Failed");
                exit(-1);
            }
            else if (pid == 0)
            {
                
            }
        }
    }
    while ( getchar() != 'q' );

    displayMainMenu();
}

/**
 * Handle the user input choice
*/
void inputHandler( int choice_ )
{
    switch( choice_ )
    {
        /*! Create a dummy process */
        case 1:
            createProcess();
            break;

        /*! Multiply matrices with a single process */
        case 2:
            matrixMultiplication(false);
            break;

        /*! Multiply matrices with multiple processes */
        case 3:
            matrixMultiplication(true);
            break;

        /*! Perform a benchmark test */
        case 4:
            benchmark();
            break;

        /*! Exit */
        case 5:
            exit(0);
            break;

        default:
            displayMainMenu();
            break;
    }
}

/**
 * Display the menu
*/
void displayMainMenu()
{
    system("clear");

    cout << endl << "==============================================================================" << endl;
    cout << "*** Priority manager v0.1 ***" << endl;
    cout << "==============================================================================" << endl << endl;
    cout << "Please make your selection:\n\n";
    cout << "1 - Create a new process\n";
    cout << "2 - Display processes priority\n";
    cout << "3 - Increase/decrease process priority\n";
    cout << "4 - Manage the process processor\n";
    cout << "5 - Quit\n";
    cout << endl << "==============================================================================" << endl << endl;
    cout << "Choose an option: ";

    inputHandler(getInput());
}

/**
 * Function to return to menu
*/
void return_to_menu()
{
    char opt;
    cout << endl << "Would you like to return to main menu? (y or n): ";
    cin >> opt;

    if ( opt == 'y' || opt == 'Y' )
        displayMainMenu();
    else
        exit(0);
}

/********************************************//**
* Main
***********************************************/
int main( void )
{
    //! Display the application menu
    displayMainMenu();

    //! Status message
    cout << "<<< Finish with success! >>>" << endl << endl;

    /*! Main return */
    return 0;
}